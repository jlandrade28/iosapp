//
//  DrawerView.swift
//  zupp
//
//  Created by MacbookAir on 21-04-20.
//  Copyright © 2020 MacbookAir. All rights reserved.
//

import SwiftUI


struct DrawerView_Previews: PreviewProvider {
    static var previews: some View {
        DrawerContent()
    }
}


struct DrawerContent: View {
    var body: some View {
        VStack{
            Image("logo")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .padding(.top, 100)
                .padding(.bottom, 25)
                .frame(width: UIScreen.main.bounds.width / 2.2)
            
            Text("Bienvenidos")
                .font(.system(size: 20))
                .bold()
                .padding(.bottom, 25)
            
            // Options drawer
            OptionMenu(nameOption: "Inicio", icon: "home_drawer")
            OptionMenu(nameOption: "Ayuda en linea", icon: "support_drawer")
            OptionMenu(nameOption: "Salir", icon: "logout_drawer")
          Spacer()
            //Bottom options
        }.background(Color(.white))
    }
}

struct NavigationDrawer: View {
    private let width = UIScreen.main.bounds.width - 100
    let isOpen: Bool
    
    var body: some View {
        ZStack{
            HStack {
                DrawerContent()
                    .frame(width: self.width)
                    .offset(x: self.isOpen ? 0 : -self.width)
                    .animation(.default)
                    .edgesIgnoringSafeArea(.all)
                Spacer()
                
            }
        }
    }
}

struct OptionMenu: View {
    
    var nameOption: String
    var icon: String
    
    var body: some View {
        HStack{
            Image(icon)
            Button(action: {
                print("Button action")
            }) {
                Text(nameOption)
                    .font(.system(size: 14))
                    .foregroundColor(Color.black)
                    .frame(maxWidth: (UIScreen.main.bounds.width - 150) - 40, alignment: .leading)
                    
            }
        }.frame(minWidth: 0, maxWidth: .infinity, maxHeight: 26)
    }
}

struct BlanketView: View{
    var color: Color
    var body: some View{
        VStack{
            Spacer()
        }
        .frame(minWidth: 0, maxWidth: .infinity, maxHeight: .infinity)
        .background(color)
        .edgesIgnoringSafeArea(.all)
    }
}
