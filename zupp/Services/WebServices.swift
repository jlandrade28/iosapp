//
//  WebServices.swift
//  zupp
//
//  Created by MacbookAir on 19-04-20.
//  Copyright © 2020 MacbookAir. All rights reserved.
//

import Foundation

class Webservice {
    
    func getPosts(completion: @escaping ([HomeData]?) -> ()) {
        
        guard let url = URL(string: "https://serverapvps.mooo.com:9292/api/v1/home") else {
            fatalError("Invalid URL")
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            let posts = try? JSONDecoder().decode([HomeData].self, from: data)
            
            DispatchQueue.main.async {
                completion(posts)
            }
            
        }.resume()
        
    }
    
}
