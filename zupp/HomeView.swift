
//
//  HomeView.swift
//  zupp
//
//  Created by MacbookAir on 18-04-20.
//  Copyright © 2020 MacbookAir. All rights reserved.
//

import SwiftUI
import RemoteImage

struct HomeView: View {

    @EnvironmentObject var settings : GeneralSetting
    
    private var numRows = 2
 
    var body: some View {
     ZStack{
        PrincipalView()
        NavigationDrawer(isOpen: self.settings.drawerIsOpen)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

struct OptionsButtonsHome: View {
    
    var titleOption: String
    var colorOption: String
    var iconOption: String
    
    var body: some View {
            //Action Button
            NavigationLink(destination: CommerceDetails()){
             VStack{
                  Circle()
                      .fill(Color(hexStringToUIColor(hex: colorOption)))
                      .cornerRadius(25)
                      .frame(width: 50, height: 50)
                    .overlay(
                        
                      RemoteImage(type: .url(URL(string: iconOption)!), errorView: { error in
                            Text(error.localizedDescription)
                        }, imageView: { image in
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 31, height: 30)
                                .foregroundColor(.white)
                        }, loadingView: {
                            Text("...")
                        })
                )
                  Text(titleOption)
                      .bold()
                      .foregroundColor(Color.black)
                      .font(.system(size: 11))
              }.frame(width: UIScreen.main.bounds.width/4.5)
            }
    }
}

struct ScrollListOptions: View {
    
   var url = URL(string: "https://yoelijocuidarme.es/wp-content/uploads/2019/01/alimentos-sanos-que-engordan-620x420@2x.jpg")!
    
    var body: some View {
        VStack{
            RemoteImage(type: .url(url), errorView: { error in
                Text(error.localizedDescription)
            }, imageView: { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: UIScreen.main.bounds.width - 100, height: 120)
                    .clipShape(Rectangle())
            }, loadingView: {
                Text("Loading ...")
            }).padding(.bottom, 0)
            
            Text("Precios imbatibles")
                .foregroundColor(Color.black)
                .bold()
                .frame(maxWidth: .infinity, alignment: .leading)
            Text("Platos económicos en tu zona")
                .font(.system(size: 10))
                .foregroundColor(Color.gray)
                .frame(maxWidth: .infinity, alignment: .leading)
            
        }.cornerRadius(2)
    }
}


struct CorouselOptions: View {
    
    var titleSection: String
    var subtitleSection: String
    
    var body: some View {
     VStack{
          VStack(alignment: .leading){
              Text(titleSection)
                .foregroundColor(Color.black)
                .bold()
                .frame(maxWidth: .infinity, alignment: .leading)
              Text(subtitleSection)
                .foregroundColor(Color.black)
                .font(.system(size: 10))
                .frame(maxWidth: .infinity, alignment: .leading)
          }
          .padding([.top, .leading], 10)
          .frame(width: UIScreen.main.bounds.width)
          ScrollView(.horizontal, showsIndicators: false) {
                HStack{
                    ScrollListOptions()
                    ScrollListOptions()
                    ScrollListOptions()
                }.frame(height: 170)
           }.padding(.horizontal)
        }
    }
}


struct Divider: View {
    var body: some View {
        Rectangle()
            .foregroundColor(Color(red: 245/255, green: 245/255, blue: 245/255))
            .frame(height: 10)
    }
}

struct ActivityIndicator: View {

  @State private var isAnimating: Bool = false
  var body: some View {

    GeometryReader { (geometry: GeometryProxy) in
      ForEach(0..<5) { index in
        Group {
          Circle()
            .frame(width: geometry.size.width / 5, height: geometry.size.height / 5)
            .scaleEffect(!self.isAnimating ? 1 - CGFloat(index) / 5 : 0.2 + CGFloat(index) / 5)
            .offset(y: geometry.size.width / 10 - geometry.size.height / 2)
          }.frame(width: geometry.size.width, height: geometry.size.height)
            .rotationEffect(!self.isAnimating ? .degrees(0) : .degrees(360))
            .animation(Animation
            .timingCurve(0.5, 0.15 + Double(index) / 5, 0.25, 1, duration: 1.5)
            .repeatForever(autoreverses: false))
        }
      }.aspectRatio(1, contentMode: .fit)
        .onAppear {
          self.isAnimating = true
        }
  }

}

struct SearchHome: View {
    var body: some View {
        VStack{
        Rectangle()
            .foregroundColor(Color(red: 242/255, green: 242/255, blue: 242/255))
            .frame(height: 42)
        }
        .cornerRadius(5)
        .padding(.horizontal)
        .overlay(
            HStack{
                Image("search_icon")
                Text("¿A dónde quiere pedir?")
                    .font(.system(size: 13))
                .foregroundColor(Color(red: 179/255, green: 186/255, blue: 191/255))
                Spacer()
            }
            .padding(.horizontal, 25)
        ).padding(.top, 10)
    }
}


private func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

struct PrincipalView: View {
    
    @ObservedObject private var ModelVIew = PostListViewModel()
    @EnvironmentObject var settings : GeneralSetting
    private var numRows = 2
    
    var body: some View {
        VStack{
            if (ModelVIew.loader) {
                ScrollView() {
                    SearchHome()
                    ForEach(0..<self.numRows) { index in
                        HStack{
                            if (index == 0) {
                                ForEach(self.ModelVIew.posts[0].optionsMenu.indices){ indx in
                                    if (indx <= 3){
                                        OptionsButtonsHome(titleOption: self.ModelVIew.posts[0].optionsMenu[indx].name, colorOption: self.ModelVIew.posts[0].optionsMenu[indx].colorHex, iconOption: self.ModelVIew.posts[0].optionsMenu[indx].icon)
                                    }
                                }
                            }
                            if (index == 1) {
                                ForEach(self.ModelVIew.posts[0].optionsMenu.indices){ indx in
                                    if (indx > 3 && indx <= 7){
                                        OptionsButtonsHome(titleOption: self.ModelVIew.posts[0].optionsMenu[indx].name, colorOption: self.ModelVIew.posts[0].optionsMenu[indx].colorHex, iconOption: self.ModelVIew.posts[0].optionsMenu[indx].icon)
                                    }
                                }
                            }
                        }.padding(.vertical, 7)
                    }
                    Divider()
                    CorouselOptions(titleSection: "Precios imbatibles", subtitleSection: "Platos econimicos en tu zona")
                    
                    Divider()
                    CorouselOptions(titleSection: "Precios imbatibles", subtitleSection: "Platos econimicos en tu zona")
                    Spacer()
                }.padding(.top,0)
                
            }
        }
    }
}
