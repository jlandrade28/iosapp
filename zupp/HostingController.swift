//
//  HostingController.swift
//  zupp
//
//  Created by MacbookAir on 18-04-20.
//  Copyright © 2020 MacbookAir. All rights reserved.
//

import Foundation
import SwiftUI

final class HostingController<T: View>: UIHostingController<T> {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}
