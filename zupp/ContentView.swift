//
//  ContentView.swift
//  zupp
//
//  Created by MacbookAir on 16-04-20.
//  Copyright © 2020 MacbookAir. All rights reserved.
//

import SwiftUI
import RemoteImage


struct ContentView: View {

    @EnvironmentObject var settings : GeneralSetting
    
    init() {
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().backgroundColor = .white
        UINavigationBar.appearance().standardAppearance.shadowColor = .clear
    }
    var body: some View {
            NavigationView {
                HomeView()
                .navigationBarTitle("",  displayMode: .inline)
                .navigationBarItems(leading: Button(action: {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.settings.drawerIsOpen.toggle()
                    }
                }) {
                    Image(systemName: "line.horizontal.3")
                        .foregroundColor(.black)
                        .font(.system(size: 25))
                    }, trailing: Button(action:{
                        
                    }) {
                        Image("location_pointer")
                           .foregroundColor(.black)
                           .font(.system(size: 25))
                })
            }
        }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
