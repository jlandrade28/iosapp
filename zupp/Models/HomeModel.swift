//
//  HomeModel.swift
//  zupp
//
//  Created by MacbookAir on 19-04-20.
//  Copyright © 2020 MacbookAir. All rights reserved.
//

import Foundation


public struct optionsMenu : Codable {
    let id: Int
    let name: String
    let active: Bool
    let colorHex: String
    let colorRgb: [Int]
    let icon: String
    enum CodingKeys : String, CodingKey {
           case id
           case name
           case active
           case colorHex
           case colorRgb
           case icon
     }
}

struct HomeData: Codable, Identifiable {
     let id = UUID()
     let optionsMenu: [optionsMenu]
    enum CodingKeys : String, CodingKey {
          case optionsMenu
    }
}
