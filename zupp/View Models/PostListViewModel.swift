//
//  PostListViewModel.swift
//  zupp
//
//  Created by MacbookAir on 19-04-20.
//  Copyright © 2020 MacbookAir. All rights reserved.
//

import Foundation
import Combine
import Alamofire

final class PostListViewModel: ObservableObject {
    @Published var posts : [HomeData] = []
    @Published var loader = false
    
    init(){
        self.fetchHome()
    }
}
  
extension PostListViewModel {
    func fetchHome() {
            AF.request("https://serverapvps.mooo.com:9292/api/v1/home")
            .validate()
            .responseDecodable(of: HomeData.self) { (response) in
                guard let home = response.value else { return }
                self.posts = [home]
                print(self.posts)
                self.loader = true
            }
        }
}


/*
struct PostViewModel {
    
    var post: HomeData
    
    init(post: HomeData) {
        self.post = post
    }
    
    var id: UUID {
        return self.post.id
    }
    
    var optionsMenu: Array<Any> {
        return self.post.optionsMenu
    }
    
}
*/
